import '~/css/main.css';
import 'react-toastify/dist/ReactToastify.css';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Dashboard } from '~/pages/private/Dashboard';
import { LinksPage } from '~/pages/private/Links';
import { RecordsPage } from '~/pages/private/Records';
import { AuthPage } from '~/pages/public/Auth';
import { NotFound } from '~/pages/public/NotFound';
import { AuthMiddleware } from '~/middleware/AuthMiddleware';
import { Logout } from '~/pages/private/Logout';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AuthMiddleware />}>
          <Route path="dashboard" element={<Dashboard />}>
            <Route path="links" element={<LinksPage />} />
            <Route path="records" element={<RecordsPage />} />
          </Route>
          <Route path="logout" element={<Logout />} />
        </Route>
        <Route path="login" element={<AuthPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
