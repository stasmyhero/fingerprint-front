import { Fingerprint, Link, Logout } from '@mui/icons-material';
import { DateTimeCell } from '~/components/RecordTable/DateTimeCell';
import { UrlCell } from '~/components/RecordTable/UrlCell';

export const recordColumns = [
  {
    field: 'dateTime',
    headerName: 'Дата',
    flex: 1,
    renderCell: ({ value }) => <DateTimeCell date={value.date} time={value.time} />,
  },
  {
    field: 'link',
    headerName: 'Ссылка',
    renderHeader: () => (
      <span style={{ lineHeight: '22px' }}>
        Ссылка и <br />
        получатель
      </span>
    ),
    renderCell: (params) => <UrlCell recipient={params.value.recipient} url={params.value.url} />,
    flex: 1,
  },
  { field: 'ip', headerName: 'IP-адрес', flex: 0.8 },
  {
    field: 'osAndBrowser',
    headerName: 'ОC и браузер',
    flex: 1,
    renderCell: ({ value }) => (
      <span>
        {value.os}
        <br />
        {value.browser}
      </span>
    ),
  },
  { field: 'location', headerName: 'Локация', flex: 1 },
  { field: 'fingerprint', headerName: 'Отпечаток', flex: 1 },
];

export const linksColumns = [
  // { field: 'id', headerName: 'ID', flex: 0.1 },
  {
    field: 'dateTime',
    headerName: 'Создана',
    flex: 0.2,
    renderCell: ({ value }) => <DateTimeCell date={value.date} time={value.time} />,
  },
  { field: 'recipient', headerName: 'Получатель', flex: 0.2 },

  { field: 'url', headerName: 'Ссылка', flex: 1, renderCell: (params) => <UrlCell url={params.value ?? ''} /> },
];

export const sidebarLinks = [
  // { label: 'Главная', url: '/dashboard', icon: <Fingerprint />, key: 'dashboard' },
  { label: 'Переходы', url: 'records', icon: <Fingerprint />, key: 'records', className: '' },
  { label: 'Ссылки', url: 'links', icon: <Link />, key: 'links', className: 'link' },
  { label: 'Выход', url: '/logout', icon: <Logout />, devider: true, key: 'logout', className: 'logout' },
];

export const dateTimeFormat = 'DD.MM.YYYY HH:mm';
