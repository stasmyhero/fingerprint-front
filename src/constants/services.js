export const tokenKey = 'jwt-token';

export const baseURL = process.env.REACT_APP_API_URL;

export const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};
