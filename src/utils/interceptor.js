import { tokenKey } from '~/constants/services';

export const authInterceptor = (config) => {
  if (localStorage.getItem(tokenKey)) {
    config.headers.Authorization = `Bearer ${localStorage.getItem(tokenKey)}`;
  } else {
    delete config.headers.Authorization;
  }
  return config;
};
