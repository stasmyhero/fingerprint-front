import dayjs from 'dayjs';
import { dateTimeFormat } from '~/constants/ui';

export const prepareLink = (link) => {
  const [date, time] = dayjs(link.date).format(dateTimeFormat).split(' ');
  link.recipient = link.recipient || 'Не указан';
  link.dateTime = { date, time };
  return link;
};

export const prepareLinks = (links) => {
  if (links && Array.isArray(links)) {
    return links.map(prepareLink).sort((a, b) => dayjs(b.date) - dayjs(a.date));
  }
};

export const prepareRecords = (records) => {
  if (records && Array.isArray(records)) {
    return records
      .map((record) => {
        const [date, time] = dayjs(record.isoDate).format(dateTimeFormat).split(' ');
        record.dateTime = record.isoDate ? { date, time } : { date: '-', time: '-' };
        record.location = [record.city || '-', record.country || '-'].join(', ');
        if (record?.browser === undefined) record.browser = '';
        record.osAndBrowser = {
          os: record.os?.replace('версия:', '') ?? '-',
          browser: record.browser?.replace('версия:', '') ?? '-',
        };
        const recipient = record.recipient ?? 'Не указан';
        record.link = { url: record.link ?? '', recipient };
        record.fingerprint = record.fingerprint ?? '';
        return record;
      })
      .sort((a, b) => dayjs(b.isoDate) - dayjs(a.isoDate));
  }
};
