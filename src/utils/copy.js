export const copy = (text, isSafari) => {
  let textArea;

  const createTextArea = (text) => {
    textArea = document.createElement('textArea');
    textArea.value = text;
    document.body.appendChild(textArea);
  };

  const selectText = () => {
    let range, selection;

    if (isSafari) {
      range = document.createRange();
      range.selectNodeContents(textArea);
      selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
      textArea.setSelectionRange(0, 999999);
    } else {
      textArea.select();
    }
  };

  const copyToClipboard = () => {
    document.execCommand('copy');
    document.body.removeChild(textArea);
  };

  createTextArea(text);
  selectText();
  copyToClipboard();
};
