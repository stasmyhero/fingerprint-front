import { useState } from 'react';

export const useFetch = (fetcher) => {
  const [isLoading, setLoading] = useState();
  const [isError, setError] = useState();
  const fetching = async () => {
    try {
      setLoading(true);
      await fetcher();
      setLoading(false);
    } catch (e) {
      setError(true);
      setLoading(false);
    } finally {
      setLoading(false);
    }
  };

  return [fetching, isLoading, isError];
};
