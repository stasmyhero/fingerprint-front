import { useState } from 'react';

export const useToggle = (visible) => {
  const [isOpen, setIsOpen] = useState(visible);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const hide = () => {
    setIsOpen(false);
  };

  const show = () => {
    setIsOpen(true);
  };

  return [isOpen, setIsOpen, toggle];
};
