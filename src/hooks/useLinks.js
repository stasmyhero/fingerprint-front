import { useState } from 'react';
import { LinkService } from '~/services/links';
import { prepareLink, prepareLinks } from '~/services/utils/common';
import { useFetch } from './useFetch';

export const useLinks = () => {
  const [links, setLinks] = useState([]);
  const [fetchLinks, isLoading, isError] = useFetch(async () => {
    const links = await LinkService.getLinks();
    setLinks(prepareLinks(links));
  });
  const deleteLink = async (id) => {
    const result = await LinkService.delete(id);
    return result;
  };
  const generateLink = async (linkData) => {
    const link = await LinkService.generate(linkData);
    if (link) {
      setLinks([prepareLink(link), ...links]);
      return link;
    } else {
      throw new Error('Не удалось сгенерировать ссылку');
    }
  };

  return [links, setLinks, fetchLinks, generateLink, deleteLink, isLoading, isError];
};
