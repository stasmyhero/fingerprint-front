import axios from 'axios';
import { toast } from 'react-toastify';
import { baseURL, headers } from '~/constants/services';
import { authInterceptor } from '~/utils/interceptor';

export const $api = axios.create({
  withCredentials: true,
  baseURL,
  headers,
});

$api.interceptors.request.use(authInterceptor);
$api.onError(async (err) => {
  toast(`${err.code}: ${err.message}`);
});
