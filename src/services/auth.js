import axios from 'axios';
import { baseURL, tokenKey, headers } from '~/constants/services';
import { $api } from './api';

export class AuthService {
  setToken(token) {
    localStorage.setItem(tokenKey, token);
  }

  getToken() {
    localStorage.getItem(tokenKey);
  }

  deleteToken() {
    localStorage.removeItem(tokenKey);
  }

  static async login(email, password) {
    const res = await $api.post('/auth/login', { email, password });
    if (res.status === 200 && res.data?.accessToken) {
      this.setToken(res.data.accessToken);
    }
  }

  static async logout() {
    const res = await $api.post('/auth/logout/');
    if (res.status === 200) {
      this.deleteToken();
    } else {
      throw new Error();
    }
  }

  static async checkAuth() {
    const token = localStorage.getItem(tokenKey);
    try {
      if (token) {
        const res = await axios.get('/auth/refresh', { baseURL: baseURL, withCredentials: true, headers });
        if (res.data?.accessToken) {
          this.setToken(tokenKey, res.data.accessToken);
          return true;
        }
      }
      this.deleteToken();
      return false;
    } catch (err) {
      return false;
    }
  }
}
