import { $api } from './api';

export class RecordService {
  static getRecords = async (page = 1, limit = 100) => {
    const res = await $api.get(`/record/all?page=${page}&limit=${limit}`);
    if (res.status === 200) {
      return res.data;
    } else throw new Error();
  };

  static getRecord = async (id) => {
    return $api.$get(`/record?id=${id}`);
  };

  static deleteRecord = async (id) => {
    return $api.delete(`/record?id=${id}`);
  };
}
