import { $api } from './api';

export class LinkService {
  static async getLinks(page = 1, limit = 100) {
    const res = await $api.get(`/link/all`);
    if (res.status === 200) {
      return res.data;
    } else return [];
  }

  static async getLink() {
    const res = await $api.$get(`/link?id=${id}`);
    if (res.status === 200) {
      return res.data;
    } else {
      return null;
    }
  }

  static async generate({ recipient, isoDate, timestamp, postLink }) {
    const res = await $api.post('/link/generate', {
      isoDate,
      timestamp,
      recipient,
      postLink,
    });
    if (res.status == 200) {
      return res.data;
    } else {
      return null;
    }
  }

  static deleteRecord = async (id) => {
    return $api.delete(`/link?id=${id}`);
  };
}
