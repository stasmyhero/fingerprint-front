import { Card, CardContent } from '@mui/material';

export const MobileCard = ({ children }) => {
  return (
    <Card sx={{ mb: 2, borderRadius: '12px', padding: '8px' }}>
      <CardContent sx={{ padding: '8px', paddingBottom: '8px !important' }}>{children}</CardContent>
    </Card>
  );
};
