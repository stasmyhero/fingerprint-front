export const MyChip = ({ children, maxWidth }) => {
  return (
    <span className="chip" style={{ maxWidth: maxWidth || '' }}>
      {children}
    </span>
  );
};
