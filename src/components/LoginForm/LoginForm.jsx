import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '@mui/lab';
import { TextField, Container, CssBaseline, Box } from '@mui/material';
import { AuthService } from '~/services/auth';
import { toastOptions } from '~/constants/toast';

export const LoginForm = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const onSubmit = async (event) => {
    event.preventDefault();
    if (!loading) {
      try {
        setLoading(true);
        const data = new FormData(event.currentTarget);
        const password = data.get('password');
        const email = data.get('email');
        await AuthService.login(email, password);
        setLoading(false);
        const isLoggedIn = await AuthService.checkAuth();
        if (isLoggedIn) {
          navigate('/dashboard');
        }
      } catch (err) {
        setLoading(false);
        toast(err.message ?? 'Ошибка', { ...toastOptions, onClose: () => setLoading(false) });
      }
    }
  };

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xs">
        <h1>Авторизация</h1>
        <CssBaseline />
        <Box component="form" onSubmit={onSubmit}>
          <TextField margin="normal" required fullWidth id="email" name="email" autoFocus label="Почта" />
          <TextField type="password" id="password" name="password" required fullWidth label="Пароль" />
          <LoadingButton type="submit" fullWidth variant="contained" loading={loading} sx={{ mt: 3, mb: 2 }}>
            Войти
          </LoadingButton>
        </Box>
      </Container>
      <ToastContainer />
    </React.Fragment>
  );
};
