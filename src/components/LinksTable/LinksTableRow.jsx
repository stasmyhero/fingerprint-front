import { TableCell, TableRow } from '@mui/material';
import { DateTimeCell } from '~/component/RecordTable/DateTimeCell';
import { UrlCell } from '~/components/RecordTable/UrlCell';

export const LinksTableRow = ({ link }) => {
  return (
    <TableRow>
      <TableCell>
        <DateTimeCell date={link.dateTime.date} time={link.dateTime.time} />
      </TableCell>
      <TableCell>
        <UrlCell recipient={link.recipient} max-width={650} />
      </TableCell>
      <TableCell>
        <UrlCell url={link.url} max-width={650} />
      </TableCell>
    </TableRow>
  );
};
