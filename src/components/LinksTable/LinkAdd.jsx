import dayjs from 'dayjs';
import { useState } from 'react';
import { Box } from '@mui/system';
import { TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { browserName } from 'react-device-detect';
import { toast } from 'react-toastify';
import { copy } from '~/services/utils/copy';

export const LinkAdd = ({ generateLink }) => {
  const [loading, setLoading] = useState();
  const onGenerateLink = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const recipient = data.get('recipient') ?? '';
    const postLink = data.get('post') ?? '';
    const isoDate = dayjs().toISOString();
    const timestamp = Math.floor(Date.now() / 1000);
    const isSafari = browserName && browserName.toLowerCase() == 'safari';
    try {
      setLoading(true);
      const link = await generateLink({ recipient, isoDate, timestamp, postLink });
      copy(link.url, isSafari);
      toast('Ссылка скопирована в буфер обмена');
      setLoading(false);
    } catch (err) {
      toast(err?.message ?? 'Ошибка при генерации ссылки');
    } finally {
      setLoading(false);
    }
  };
  return (
    <Box component="form" onSubmit={onGenerateLink}>
      <TextField type="url" id="post" name="post" required fullWidth label="Ссылка" sx={{ mb: 1 }} />
      <TextField type="text" id="recipient" name="recipient" fullWidth label="Получатель" />
      <LoadingButton type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }} loading={loading}>
        Сгенерировать ссылку
      </LoadingButton>
    </Box>
  );
};
