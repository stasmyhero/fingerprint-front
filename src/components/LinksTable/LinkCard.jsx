import { Typography } from '@mui/material';
import { MobileCard } from '~/components/Generic/MobileCard';
import { MyChip } from '~/components/Generic/MyChip';
import { UrlCell } from '~/components/RecordTable/UrlCell';

export const LinkCard = ({ link }) => {
  return (
    <MobileCard sx={{ mb: 2 }}>
      <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem', mb: '16px' }}>
        <span className="link-cell">
          <a className="recipient">
            <MyChip>{link.recipient}</MyChip>
          </a>
        </span>
      </Typography>
      <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem', mb: '16px' }}>
        <UrlCell url={link.url} />
      </Typography>
      <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem', sx: '16px', color: 'var(--black32)' }}>
        {link.dateTime.date} {link.dateTime.time}
      </Typography>
    </MobileCard>
  );
};
