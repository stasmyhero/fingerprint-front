import React, { useEffect } from 'react';
import {
  Container,
  Box,
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Card,
  CardContent,
  Grid,
  CardHeader,
} from '@mui/material';
import { AppLoader } from '~/AppLoader/AppLoader';
import { linksColumns } from '~/constants/ui';
import { useLinks } from '~/hooks/useLinks';
import { LinkAdd } from './LinkAdd';
import { LinkCard } from './LinkCard';
import { LinksTableRow } from './LinksTableRow';

export const LinksTable = () => {
  const [links, _setLinks, fetchLinks, generateLink, _deleteLink, isLoading, isError] = useLinks();

  useEffect(async () => {
    await fetchLinks();
  }, []);

  if (isError) {
    return (
      <Container maxWidth="xs">
        <div>Ошибка загрузки списка ссылок</div>
      </Container>
    );
  }

  if (isLoading) {
    return (
      <Container maxWidth="xs">
        <AppLoader />
      </Container>
    );
  }

  const RenderTable = () => {
    if (!links.length) {
      return '';
    }
    return (
      <Grid item md={7}>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                {linksColumns.map(({ headerName, field }) => (
                  <TableCell key={field} sx={{ color: field === 'dateTime' ? 'rgba(0, 0, 0, 0.32)' : '' }}>
                    {headerName}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {links.map((link) => (
                <LinksTableRow link={link} key={link.id} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    );
  };

  return (
    <React.Fragment>
      <Box display={{ xs: 'none', md: 'flex' }}>
        <Grid container columnSpacing={2} rowSpacing={1}>
          <RenderTable />
          <Grid item md={5} xs={12}>
            <Card variant="outlined">
              <CardHeader title="Добавить новую"></CardHeader>
              <CardContent>
                <LinkAdd generateLink={generateLink} />
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Box>
      <Box display={{ xs: 'block', md: 'none' }}>
        <LinkAdd generateLink={generateLink} />
        <Box sx={{ mt: '6px' }}>
          {links.map((link) => (
            <LinkCard link={link} key={link.id} />
          ))}
        </Box>
      </Box>
    </React.Fragment>
  );
};
