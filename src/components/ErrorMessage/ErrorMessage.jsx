import { Alert, Snackbar } from '@mui/material';
import { useEffect } from 'react';
import { useToggle } from '~/hooks/useToggle';

export const ErrorMessage = ({ message, close }) => {
  const [visible, setVisible, _, hide] = useToggle(false);

  useEffect(() => {
    setVisible(!!message);
  }, [message]);

  return (
    <Snackbar
      open={visible}
      anchorOrigin={{ horizontal: 'center', vertical: 'top' }}
      autoHideDuration={2000}
      onClose={hide}
    >
      <Alert severity="error">Ошибка: {message}</Alert>
    </Snackbar>
  );
};
