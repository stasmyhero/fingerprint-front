import { Typography } from '@mui/material';
import React from 'react';

export const PageTitle = ({ title }) => (
  <React.Fragment>
    <Typography variant="h4" sx={{ mt: 2.7, mb: 2 }} display={{ xs: 'block', md: 'none' }}>
      {title}
    </Typography>
    <Typography variant="h2" sx={{ mt: 4.7, mb: 3.2 }} display={{ xs: 'none', md: 'block' }}>
      {title}
    </Typography>
  </React.Fragment>
);
