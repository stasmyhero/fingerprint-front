import { Typography, Grid } from '@mui/material';
import { Fingerprint } from '@mui/icons-material';
import { MobileCard } from '~/components/Generic/MobileCard';
import { UrlCell } from '~/components/RecordTable/UrlCell';

export const RecordCard = ({ record }) => {
  return (
    <MobileCard sx={{ mb: 2, borderRadius: '20px', padding: '8px' }}>
      <Grid container columnSpacing={0.8} rowSpacing="16px">
        <Grid item xs={12} sx={{ mb: '8px', fontSize: '14px' }}>
          <UrlCell url={record.link.url} recipient={record.link.recipient} maxWidth="150px" />
        </Grid>
        <Grid item xs={12}>
          <Typography sx={{ fontSize: 32, fontWeight: 700 }}>{record.ip}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem' }}>{record.os}</Typography>
          <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem' }}>{record.browser}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem' }}>{record.location}</Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem' }} display={{ xs: 'flex' }} color="var(--black32)">
            <span style={{ marginRight: '7px' }}>{record.dateTime.time}</span>
            <span>{record.dateTime.date}</span>
          </Typography>
        </Grid>
        <Grid item xs={7}>
          <Typography sx={{ fontSize: 14, letterSpacing: '-0.01rem' }} display={{ xs: 'flex' }} color="var(--black32)">
            <Fingerprint fontSize="small" sx={{ mb: '2px' }} color="var(--black32)" />
            <span>{record.fingerprint}</span>
          </Typography>
        </Grid>
      </Grid>
    </MobileCard>
  );
};
