import { Box, Typography } from '@mui/material';

export const AppFooter = () => {
  const currentYear = new Date(Date.now()).getFullYear();
  return (
    <Box className="footer">
      <Typography>&copy; {currentYear}</Typography>
    </Box>
  );
};
