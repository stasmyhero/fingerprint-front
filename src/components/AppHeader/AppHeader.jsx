import { AppBar, Toolbar, Box } from '@mui/material';
import { AppHeaderMenuItem } from './AppHeaderMenuItem';

export const AppHeader = ({ links }) => {
  if (links.length)
    return (
      <Box component="nav" sx={{ paddingLeft: 0 }}>
        <AppBar position="static">
          <Toolbar>
            {links.map((link) => (
              <AppHeaderMenuItem item={link} key={link.id} />
            ))}
          </Toolbar>
        </AppBar>
      </Box>
    );
};
