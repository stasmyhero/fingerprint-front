import { Button } from '@mui/material';
import { useMatch, useResolvedPath } from 'react-router-dom';
import { Link } from 'react-router-dom';

export const AppHeaderMenuItem = ({ item, key }) => {
  if (item) {
    const resolvedPath = useResolvedPath(item.url);
    const isActive = !!useMatch({ path: resolvedPath.pathname, end: true });
    if (item)
      return (
        <Button key={key} variant="text" className={`menu-button ${item.className} ${isActive ? 'active' : ''}`}>
          <Link to={item.url}>
            <div className="menu-button_icon">{item.icon}</div>
            {item.label}
          </Link>
        </Button>
      );
  }
};
