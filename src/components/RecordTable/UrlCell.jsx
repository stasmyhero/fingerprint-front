import { Link } from '@mui/icons-material';
import { toast } from 'react-toastify';
import { toastOptions } from '~/constants/toast';
import { copy } from '~/services/utils/copy';
import { MyChip } from '~/Generic/MyChip';

export const UrlCellRecipient = ({ recipient }) =>
  recipient ? (
    <a className="recipient">
      <MyChip>{recipient}</MyChip>
    </a>
  ) : (
    ''
  );

const onClick = (url) => {
  copy(url);
  toast('Ссылка скопирована в буфер обмена', toastOptions);
};

export const UrlCell = ({ url, recipient, maxWidth }) => {
  return (
    <span className="url-cell">
      <UrlCellRecipient recipient={recipient} />
      {url ? (
        <a
          href={url}
          className="url"
          onClick={(e) => {
            e.preventDefault();
            handleClick(url);
          }}
        >
          <MyChip maxWidth={maxWidth}>
            <Link fontSize="small" sx={{ mt: '2px', ml: '-4px' }} />
            ...
            {url.replace(process.env.REACT_APP_LINK_BASE, '').split('/').splice(2).join('/')}
          </MyChip>
        </a>
      ) : (
        ''
      )}
    </span>
  );
};
