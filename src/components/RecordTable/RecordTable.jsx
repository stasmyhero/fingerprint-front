import React, { useEffect, useState } from 'react';
import { Container, Box, Table, TableContainer, TableHead, TableCell, TableBody, TableRow } from '@mui/material';
import { useFetch } from '~/hooks/useFetch';
import { recordColumns } from '~/constants/ui';
import { RecordService } from '~/services/records';
import { prepareRecords } from '~/services/utils/common';
import { RecordCard } from '~/RecordCard/RecordCard';
import { RecordTableRow } from './RecordTableRow';
import { AppLoader } from '~/components/AppLoader/AppLoader';

export const RecordTable = () => {
  const [records, setRecords] = useState([]);
  const [fetchRecords, isLoading, isError] = useFetch(async () => {
    const records = await RecordService.getRecords();
    setRecords(prepareRecords(records));
  });

  useEffect(() => {
    fetchRecords();
  }, []);

  if (isError) {
    return (
      <Container>
        <div>Ошибка загрузки записей</div>
      </Container>
    );
  }

  if (isLoading) {
    return (
      <Container maxWidth="xs">
        <AppLoader />
      </Container>
    );
  }

  if (records.length) {
    return (
      <React.Fragment>
        <Box display={{ xs: 'none', md: 'flex' }}>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  {recordColumns.map(({ headerName, field }) => (
                    <TableCell key={field} sx={{ color: field === 'dateTime' ? 'rgba(0, 0, 0, 0.32)' : '' }}>
                      {headerName}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {records.map((record) => (
                  <RecordTableRow record={record} key={record.id} />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
        // mobile view
        <Box display={{ xs: 'block', md: 'none' }}>
          {records.map((record) => (
            <RecordCard record={record} key={record.id} />
          ))}
        </Box>
      </React.Fragment>
    );
  } else {
    return <Box>Пока нет переходов</Box>;
  }
};
