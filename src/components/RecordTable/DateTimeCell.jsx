export const DateTimeCell = ({ date, time }) => (
  <span style={{ color: 'rgba(0, 0, 0, 0.32)' }}>
    {time}
    <br />
    {date}
  </span>
);
