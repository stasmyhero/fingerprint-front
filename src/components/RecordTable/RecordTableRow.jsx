import { TableCell, TableRow } from '@mui/material';
import { DateTimeCell } from './DateTimeCell';
import { UrlCell } from './UrlCell';

export const RecordTableRow = ({ record }) => {
  return (
    <TableRow>
      <TableCell>
        <DateTimeCell date={record.dateTime.date} time={record.dateTime.time} />
      </TableCell>
      <TableCell>
        <UrlCell url={record.link.url} recipient={record.link.recipient} />
      </TableCell>
      <TableCell sx={{ fontSize: 24 }}>{record.ip}</TableCell>
      <TableCell>
        <span>
          {record.osAndBrowser.os}
          <br />
          {record.osAndBrowser.browser}
        </span>
      </TableCell>
      <TableCell>{record.location}</TableCell>
      <TableCell>{record.fingerprint}</TableCell>
    </TableRow>
  );
};
