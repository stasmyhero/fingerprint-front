import { ListItem, List, Divider } from '@mui/material';

import React from 'react';
import { SidebarMenuLink } from './SidbarMenuLink';

export const SidebarMenu = ({ links, onClick }) => {
  if (Array.isArray(links)) {
    return (
      <List>
        {links.map((link) => {
          return (
            <ListItem key={link.key} disablePadding className="drawer-link">
              {link.divider ? <Divider /> : ''}
              <SidebarMenuLink link={link} onClick={onClick} />
            </ListItem>
          );
        })}
      </List>
    );
  }
};
