import { Drawer } from '@mui/material';
import { SidebarMenu } from './SiebarMenu';

export const Sidebar = ({ links, opened, onClose }) => {
  return (
    <Drawer open={opened} onClose={onClose}>
      <SidebarMenu links={links} onClick={onClose} />
    </Drawer>
  );
};
