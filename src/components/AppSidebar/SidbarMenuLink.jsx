import { useMatch, useResolvedPath } from 'react-router-dom';
import { ListItemIcon, ListItemButton, ListItemText } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';

export const SidebarMenuLink = ({ link, onClick }) => {
  const resolvedPath = useResolvedPath(link.url);
  const selected = !!useMatch({ path: resolvedPath.pathname, end: true });
  return (
    <RouterLink to={link.url}>
      <ListItemButton selected={selected} onClick={onClick}>
        <ListItemIcon>{link.icon}</ListItemIcon>
        <ListItemText primary={link.label}></ListItemText>
      </ListItemButton>
    </RouterLink>
  );
};
