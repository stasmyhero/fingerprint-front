import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthService } from '~/services/auth';

export const Logout = () => {
  const navigate = useNavigate();
  useEffect(async () => {
    await AuthService.logout();
    navigate('/login');
  });

  return <div />;
};
