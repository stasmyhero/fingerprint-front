import React from 'react';
import { LinksTable } from '~/components/LinksTable/LinksTable';
import { PageTitle } from '~/components/PageTitle/PageTitle';

export const LinksPage = () => {
  return (
    <React.Fragment>
      <PageTitle title="Ссылки" />
      <LinksTable />
    </React.Fragment>
  );
};
