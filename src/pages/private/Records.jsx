import { Typography } from '@mui/material';
import React from 'react';
import { PageTitle } from '~/components/PageTitle/PageTitle';
import { RecordTable } from '~/components/RecordTable/RecordTable';

export const RecordsPage = () => {
  return (
    <React.Fragment>
      <PageTitle title="Переходы" />
      <RecordTable />
    </React.Fragment>
  );
};
