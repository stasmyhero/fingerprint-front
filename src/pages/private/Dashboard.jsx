import React from 'react';
import { Outlet } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { AppFooter } from '~/components/AppFooter/AppFooter';
import { AppHeader } from '~/components/AppHeader/AppHeader';
import { sidebarLinks } from '~/constants/ui';

export const Dashboard = () => {
  return (
    <React.Fragment>
      <AppHeader links={sidebarLinks} />
      <div className="page-wrapper">
        <Outlet />
        <ToastContainer />
      </div>
      <AppFooter />
    </React.Fragment>
  );
};
