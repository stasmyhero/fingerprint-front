import { Container } from '@mui/material';

export const NotFound = () => (
  <Container>
    <h1>404</h1>
    <div>NOT FOUND</div>
    <div>Страница не найдена</div>
  </Container>
);
