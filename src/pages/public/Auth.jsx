import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { LoginForm } from '~/components/LoginForm/LoginForm';
import { AuthService } from '~/services/auth';

export const AuthPage = () => {
  const navigate = useNavigate();
  useEffect(async () => {
    const isLoggendIn = await AuthService.checkAuth();
    if (isLoggendIn) {
      navigate('/dashboard/records');
      return;
    }
  }, []);
  return <LoginForm />;
};
