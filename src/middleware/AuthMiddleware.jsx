import { useEffect } from 'react';
import { Outlet, useNavigate, useLocation } from 'react-router-dom';
import { AuthService } from '~/services/auth';

export const AuthMiddleware = () => {
  const navigate = useNavigate();
  const location = useLocation();
  useEffect(async () => {
    const result = await AuthService.checkAuth();
    if (!result) {
      navigate('/login');
      return;
    }
    if ((location.pathname = '/')) {
      navigate('/dashboard/records');
      return;
    }
  }, []);

  return (
    <div>
      <Outlet />
    </div>
  );
};
